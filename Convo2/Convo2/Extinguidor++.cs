﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class Extinguidor__ : Form
    {
        public Extinguidor__()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GestionExtinguidor gte = new GestionExtinguidor();

            gte.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
