﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convo2.entities
{
    class Factura
    {
        private int id;
        private string cod_factura;
        private Extinguidores extinguidores;
        private string total;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        internal Extinguidores Extinguidores
        {
            get
            {
                return extinguidores;
            }

            set
            {
                extinguidores = value;
            }
        }

        public string Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public Factura(int id, string cod_factura, Extinguidores extinguidores, string total)
        {
            this.Id = id;
            this.Cod_factura = cod_factura;
            this.Extinguidores = extinguidores;
            this.Total = total;
        }
    }
}
