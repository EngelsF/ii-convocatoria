﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convo2.entities
{
    class RpFactura
    {
        private string cod_factura;
        private int id_extinguidor;
        private int cantidad;

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public int Id_extinguidor
        {
            get
            {
                return id_extinguidor;
            }

            set
            {
                id_extinguidor = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public RpFactura(string cod_factura, int id_extinguidor, int cantidad)
        {
            this.Cod_factura = cod_factura;
            this.Id_extinguidor = id_extinguidor;
            this.Cantidad = cantidad;
        }
    }
}
