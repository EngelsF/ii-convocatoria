﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convo2.entities
{
    class Extinguidores
    {
        private string id;
        private string categoria;
        private string marca;
        private string capacidad;
        private string tipo_extinguidor;
        private string unidad_medida;
        private string cantidad;

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Tipo_extinguidor
        {
            get
            {
                return tipo_extinguidor;
            }

            set
            {
                tipo_extinguidor = value;
            }
        }

        public string Unidad_medida
        {
            get
            {
                return unidad_medida;
            }

            set
            {
                unidad_medida = value;
            }
        }

        public string Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public Extinguidores(string id, string categoria, string marca, string capacidad, string tipo_extinguidor, string unidad_medida, string cantidad)
        {
            this.id = id;
            this.categoria = categoria;
            this.marca = marca;
            this.capacidad = capacidad;
            this.tipo_extinguidor = tipo_extinguidor;
            this.unidad_medida = unidad_medida;
            this.cantidad = cantidad;
        }
    }
}
