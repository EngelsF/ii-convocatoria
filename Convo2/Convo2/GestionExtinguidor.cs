﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class GestionExtinguidor : Form
    {
        private DataSet dsExtinguidor;
        private BindingSource bsExtinguidor;

        public DataSet DsClientes
        {
            get
            {
                return dsExtinguidor;
            }

            set
            {
                dsExtinguidor = value;
            }
        }
        public GestionExtinguidor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }
    }
}
