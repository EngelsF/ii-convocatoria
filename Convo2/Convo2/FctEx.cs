﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class FctEx : Form
    {
        private DataSet dsFactura;
        public FctEx()
        {
            InitializeComponent();
        }

        public DataSet DsFactura
        {
            get
            {
                return dsFactura;
            }

            set
            {
                dsFactura = value;
            }
        }

        private void FctEx_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
           
        }
    }
}
