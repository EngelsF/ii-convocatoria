﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convo2
{
    public partial class Form1 : Form
    {
        //private DataTable dtExtinguidor;

        public Form1()
        {
            InitializeComponent();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Extinguidor__ ex = new Extinguidor__();
            ex.Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void facturarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FctEx fe = new FctEx();
            fe.Show();
        }

        
    }
}
